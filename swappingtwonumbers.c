#include<stdio.h>
void swap(int*a,int*b);
int main()
{
    int a, b;
    printf("enter a and b\n");
    scanf("%d %d",&a,&b);
    printf("numbers before swapping a=%d b=%d",a,b);
    swap(&a,&b);
    printf("swapped numbers area a=%d b=%d",a,b);
    return 0;
}
void swap(int*x,int*y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}